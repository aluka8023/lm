#!/bin/bash
echo "Start..."
nohup java -jar lm.jar \
	-Xms256m \
	-Xmx1024m \
	-Dserver.port=80 \
	> lm.out 2>&1 &
	
count=`ps -ef |grep java|grep lm.jar|grep -v grep|wc -l`
if [ $count != 0 ];then
    echo "Start success!" 
fi