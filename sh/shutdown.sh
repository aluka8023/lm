#!/bin/bash
echo "Stoping..." 
boot_id=`ps -ef |grep java|grep lm.jar|grep -v grep|awk '{print $2}'`
count=`ps -ef |grep java|grep lm.jar|grep -v grep|wc -l`
if [ $count != 0 ];then
    kill $boot_id
    count=`ps -ef |grep java|grep lm.jar|grep -v grep|wc -l`

    boot_id=`ps -ef |grep java|grep lm.jar|grep -v grep|awk '{print $2}'`
    kill -9 $boot_id

    echo "Stoping success!" 
fi