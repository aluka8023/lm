var Login = {};
(function () {
    layui.use(['carousel','form'], function () {
        var carousel = layui.carousel;
        var form = layui.form;

        //   判断是否在iframe中, 如果是，刷新父页面
        if(window.top!==window.self){window.top.location=window.location}

        carousel.render({
            elem: '#div-carousel',
            width: '100%',
            height: '100%',
            anim: 'fade',
            arrow: 'none',
            indicator: 'none'
        });
    });
    var LoginFn = function(){
        var FormData = {
            "username": $("#username").val(),
            "password": $("#password").val(),
            "rememberMe": $("#rememberMe")[0].checked
        }
        $.ajax({
            type: "post",
            url: "/login",
            data: FormData,
            success: function(r) {
                if (r.status === 200) {
                    setCookie("access_token", r.token, r.expire);
                    setCookie("userName", r.customField.userName, r.expire);
                    location.href = '/';
                } else {
                    layer.msg(r.message);
                }
            }
        });
    };
    $("#loginButton").click(LoginFn);

    window.onkeyup = function(e){
        if(e.code === "Enter"){
            LoginFn();
        }
    };
    var setCookie = function (name, value, expire) {
        var expireDate = new Date(expire);
        document.cookie = name + "=" + escape(value) + ";expires=" + expireDate.toGMTString() + ";path=/";
    };
})(Login);
