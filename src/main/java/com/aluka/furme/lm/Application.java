package com.aluka.furme.lm;

import com.aluka.nirvana.framework.core.annotation.EnableNirvana;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * 浪牡个人平台启动类
 * @author gongli
 * @since 2020-8-2 10:45
 */
@SpringBootApplication
@EnableNirvana
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
