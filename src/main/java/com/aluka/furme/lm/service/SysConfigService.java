package com.aluka.furme.lm.service;

import com.aluka.furme.lm.configuration.GlobalDaoConfiguration;
import com.aluka.furme.lm.entity.SysConfig;
import com.aluka.furme.lm.service.common.BaseService;
import org.springframework.stereotype.Service;

@Service
public class SysConfigService extends BaseService<SysConfig> {

    public SysConfigService() {
        super(GlobalDaoConfiguration.FileBean.CONFIG);
    }

}
