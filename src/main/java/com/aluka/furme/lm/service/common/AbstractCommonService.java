package com.aluka.furme.lm.service.common;


import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import com.aluka.furme.lm.configuration.GlobalDaoConfiguration;
import com.aluka.furme.lm.entity.BaseEntity;
import com.aluka.furme.lm.utils.JsonFileUtil;

import java.io.FileNotFoundException;

public abstract class AbstractCommonService {

    protected static final String DATA = "data";

    /**
     * 获取数据文件的路径，如果文件不存在，则创建一个
     *
     * @param filename 文件名
     * @return path
     */
    protected String getDataFilePath(String filename) {
        String filePath = String.format("%s/%s",
                GlobalDaoConfiguration.getProfilePath(),
                filename);
        return FileUtil.normalize(filePath);
    }

    /**
     * 保存数据对象
     *
     * @param filename 文件名
     * @param entity    对象
     */
    protected void save(String filename, BaseEntity entity) {
        String key = entity.getId();
        // 读取文件，如果存在记录，则抛出异常
        JSONObject allData = getJSONObject(filename);
        if (allData == null) {
            allData = new JSONObject();
        }
        // 判断是否存在数据
        JSONObject data = allData.getJSONObject(key);
        if (null != data && !data.isEmpty()) {
            throw new RuntimeException(String.format("%s[%s:%s]","数据已经存在:",filename,key));
        } else {
            allData.put(key, entity.toJson());
            JsonFileUtil.saveJson(getDataFilePath(filename), allData);
        }
    }

    /**
     * 修改数据对象
     *
     * @param filename 文件名
     * @param entity     对象
     */
    protected void update(String filename, BaseEntity entity) {
        String key = entity.getId();
        // 读取文件，如果不存在记录，则抛出异常
        JSONObject allData = getJSONObject(filename);
        JSONObject data = allData.getJSONObject(key);

        // 判断是否存在数据
        if (null == data || data.isEmpty()) {
            throw new RuntimeException(String.format("数据不存在:[%s:%s]",filename,key));
        } else {
            allData.put(key, entity.toJson());
            JsonFileUtil.saveJson(getDataFilePath(filename), allData);
        }
    }

    /**
     * 删除数据对象
     *
     * @param filename 文件
     * @param key      key
     */
    protected void delete(String filename, String key) {
        // 读取文件，如果存在记录，则抛出异常
        JSONObject allData = getJSONObject(filename);
        JSONObject data = allData.getJSONObject(key);
        // 判断是否存在数据
        if (CollUtil.isEmpty(data)) {
            throw new RuntimeException(String.format("不存在数据:[%s:%s]",filename,key));
        } else {
            allData.remove(key);
            JsonFileUtil.saveJson(getDataFilePath(filename), allData);
        }
    }

    /**
     * 读取整个json文件
     *
     * @param filename 文件名
     * @return json
     */
    protected JSONObject getJSONObject(String filename) {
        try {
            String filePath = getDataFilePath(filename);
            return (JSONObject) JsonFileUtil.readJson(filePath);
        } catch (FileNotFoundException e) {
            return null;
        }
    }

    protected <T> T getJsonObjectById(String file, String id, Class<T> cls) {
        if (StrUtil.isEmpty(id)) {
            return null;
        }
        JSONObject jsonObject = getJSONObject(file);
        if (jsonObject == null) {
            return null;
        }
        jsonObject = jsonObject.getJSONObject(id);
        if (jsonObject == null) {
            return null;
        }
        return jsonObject.toBean(cls);
    }
}
