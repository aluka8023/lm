package com.aluka.furme.lm.service;

import cn.hutool.core.util.CharsetUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.aluka.furme.lm.configuration.GlobalDaoConfiguration;
import com.aluka.furme.lm.entity.SysConfig;
import com.aluka.furme.lm.entity.SysNavGroup;
import com.aluka.furme.lm.entity.SysNavItem;
import com.aluka.furme.lm.service.common.BaseService;
import com.aluka.nirvana.framework.core.model.ResponseBaseEntity;
import com.aluka.nirvana.framework.security.utils.SecurityUtils;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import org.apache.catalina.security.SecurityUtil;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class SysNavService extends BaseService<SysNavGroup> {

    public SysNavService() {
        super(GlobalDaoConfiguration.FileBean.NAV);
    }

    public List<SysNavGroup> findByLoginUser(){
        String userId = SecurityUtils.getUserId();
        List<SysNavGroup> groups = Lists.newArrayList(Collections2.filter(list(), group -> {
            assert group != null;
            group.getNavItems().sort(Comparator.comparing(SysNavItem::getSort));
            return userId.equals(group.getUserId());
        }));
        groups.sort(Comparator.comparing(SysNavGroup::getSort));
        return groups;
    }

    public ResponseBaseEntity saveGroup(SysNavGroup navGroup){
        if(StringUtils.isNotBlank(navGroup.getId())){
            SysNavGroup group = getItem(navGroup.getId());
            group.setIcon(navGroup.getIcon());
            group.setName(navGroup.getName());
            group.setId(navGroup.getId());
            group.setSort(navGroup.getSort());
            updateItem(group);
        }else{
            navGroup.buildId();
            navGroup.setUserId(SecurityUtils.getUsername());
            navGroup.setSort(0);
            addItem(navGroup);
        }
        return ResponseBaseEntity.success();
    }

    public ResponseBaseEntity saveItem(SysNavItem navItem){
        SysNavGroup group = getItem(navItem.getGroupId());
        if(StringUtils.isNotBlank(navItem.getId())){
            for (SysNavItem item : group.getNavItems()) {
                if(item.getId().equals(navItem.getId())){
                    BeanUtils.copyProperties(navItem, item);
                    break;
                }
            }
        }else{
            navItem.buildId();
            group.getNavItems().add(navItem);
        }
        updateItem(group);
        return ResponseBaseEntity.success();
    }

    public ResponseBaseEntity delGroup(SysNavGroup navGroup) {
        deleteItem(navGroup.getId());
        return ResponseBaseEntity.success();
    }

    public ResponseBaseEntity delItem(SysNavItem navItem) {
        SysNavGroup navGroup = getItem(navItem.getGroupId());
        for (SysNavItem item : navGroup.getNavItems()) {
            if(item.getId().equals(navItem.getId())){
                navGroup.getNavItems().remove(item);
                break;
            }
        }
        updateItem(navGroup);
        return ResponseBaseEntity.success();
    }

    public ResponseBaseEntity init(String user) throws IOException {
        ClassPathResource navResource = new ClassPathResource("config/default-nav.json");
        String navInfo = IOUtils.toString(navResource.getInputStream(), Charsets.toCharset(CharsetUtil.UTF_8));
        JSONObject navJson = JSONUtil.parseObj(navInfo);
        List<SysNavGroup> groups = new ArrayList<>();
        for (String key : navJson.keySet()) {
            groups.add(navJson.getJSONObject(key).toBean(SysNavGroup.class));
        }
        groups.forEach(group -> {
            group.buildId();
            group.setUserId(user);
            group.getNavItems().forEach(SysNavItem::buildId);
            addItem(group);
        });
        return ResponseBaseEntity.success();
    }
}
