package com.aluka.furme.lm.service;

import cn.hutool.json.JSONObject;
import com.aluka.furme.lm.configuration.GlobalDaoConfiguration;
import com.aluka.furme.lm.entity.SysUser;
import com.aluka.furme.lm.service.common.BaseService;
import org.springframework.stereotype.Service;

@Service
public class SysUserService extends BaseService<SysUser> {

    public SysUserService() {
        super(GlobalDaoConfiguration.FileBean.USER);
    }

    public SysUser findByLoginName(String loginName){
        return getItem(loginName);
    }

    /**
     * 验证用户md5
     *
     * @param userMd5 用户md5
     * @return userModel 用户对象
     */
    public SysUser checkUser(String userMd5) {
        JSONObject jsonData = getJSONObject(GlobalDaoConfiguration.FileBean.USER);
        if (jsonData == null) {
            return null;
        }
        for (String strKey : jsonData.keySet()) {
            JSONObject jsonUser = jsonData.getJSONObject(strKey);
            SysUser userModel = jsonUser.toBean(SysUser.class);
            String strUserMd5 = userModel.getUserMd5Key();
            if (strUserMd5.equals(userMd5)) {
                return userModel;
            }
        }
        return null;
    }
}
