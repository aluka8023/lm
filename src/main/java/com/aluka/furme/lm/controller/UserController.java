package com.aluka.furme.lm.controller;

import com.aluka.furme.lm.entity.SysUser;
import com.aluka.furme.lm.service.SysNavService;
import com.aluka.furme.lm.service.SysUserService;
import com.aluka.nirvana.framework.core.model.ResponseBaseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * 用户控制层
 * @author qinpi.gl
 * @date 2021/11/20 16:37
 */
@RestController
public class UserController {

    @Autowired
    private SysUserService userService;
    @Autowired
    private SysNavService navService;

    @PostMapping("/user/save")
    public ResponseBaseEntity saveUser(SysUser sysUser) throws IOException {
        sysUser.setPassword(new BCryptPasswordEncoder().encode(sysUser.getPassword()));
        try{
            userService.addItem(sysUser);
            navService.init(sysUser.getLoginName());
        }catch (RuntimeException e){
            userService.updateItem(sysUser);
        }
        return ResponseBaseEntity.success();
    }

    @PostMapping("/user/del")
    public ResponseBaseEntity delUser(String userId) throws IOException {
        userService.deleteItem(userId);
        return ResponseBaseEntity.success();
    }


}
