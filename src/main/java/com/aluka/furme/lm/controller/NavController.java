package com.aluka.furme.lm.controller;

import com.aluka.furme.lm.entity.SysNavGroup;
import com.aluka.furme.lm.entity.SysNavItem;
import com.aluka.furme.lm.service.SysNavService;
import com.aluka.nirvana.framework.core.model.ResponseBaseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author gongli
 * @since 2020-8-10 19:48
 */
@RequestMapping(value = "/nav")
@RestController
public class NavController {

    @Autowired
    private SysNavService navService;

    @GetMapping("/group")
    public ResponseBaseEntity<List<SysNavGroup>> findGroups(){
        List<SysNavGroup> groups = navService.findByLoginUser();
        return new ResponseBaseEntity<>(groups);
    }
    @PostMapping("/group/save")
    public ResponseBaseEntity saveGroup(SysNavGroup navGroup){
        return navService.saveGroup(navGroup);
    }
    @PostMapping("/item/save")
    public ResponseBaseEntity saveItem(SysNavItem navItem){
        return navService.saveItem(navItem);
    }
    @PostMapping("/group/del")
    public ResponseBaseEntity delGroup(SysNavGroup navGroup){
        return navService.delGroup(navGroup);
    }
    @PostMapping("/item/del")
    public ResponseBaseEntity delItem(SysNavItem navItem){
        return navService.delItem(navItem);
    }

}
