package com.aluka.furme.lm.controller;

import com.aluka.furme.lm.configuration.GlobalDaoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author gongli
 * @since 2020-8-2 11:21
 */
@Controller
public class PageController {

    @GetMapping("/")
    public String index(Model model){
        model.addAttribute("iconUrl", GlobalDaoConfiguration.getConfig("iconUrl"));
        return "index";
    }

    @GetMapping("/loginPage")
    public String login(){
        return "login";
    }
}
