package com.aluka.furme.lm.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class SysNavItem extends BaseEntity {

    private String logo;

    private String url;

    private int sort;

    private String groupId;
}
