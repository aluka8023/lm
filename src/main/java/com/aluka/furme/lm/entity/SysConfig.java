package com.aluka.furme.lm.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class SysConfig extends BaseEntity {

    /** 配置键 */
    private String configKey;

    /** 配置值 */
    private String configValue;

    /** 描述信息 */
    private String description;


    @Override
    public String getId() {
        return this.configKey;
    }

}
