package com.aluka.furme.lm.entity;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import lombok.Data;

import java.io.Serializable;
import java.util.UUID;

@Data
public abstract class BaseEntity implements Serializable {

    private String id;

    private String name;

    @Override
    public String toString() {
        return JSONUtil.toJsonStr(this);
    }

    public JSONObject toJson() {
        return JSONUtil.parseObj(this);
    }

    public void buildId(){
        this.id = UUID.randomUUID().toString().replace("-","");
    }
}
