package com.aluka.furme.lm.entity;

import cn.hutool.crypto.SecureUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class SysUser extends BaseEntity {

    /** 登录名称 */
    private String loginName;

    /** 用户名称 */
    private String userName;

    /** 密码 */
    private String password;

    /** 帐号状态（1正常 0停用） */
    private String status;

    /** 管理员 */
    private boolean admin;

    /** 角色信息*/
    private List<String> roles;

    @Override
    public String getId() {
        return String.valueOf(this.loginName);
    }

    public String getUserMd5Key() {
        return SecureUtil.md5(String.format("%s:%s", getId(), password));
    }
}
