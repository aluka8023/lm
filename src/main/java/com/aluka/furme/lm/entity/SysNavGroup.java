package com.aluka.furme.lm.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class SysNavGroup extends BaseEntity {

    /** 图标 */
    private String icon;

    private String userId;

    private int sort;

    private List<SysNavItem> navItems = new LinkedList<>();
}
